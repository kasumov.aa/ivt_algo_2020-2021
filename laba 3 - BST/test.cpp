#include <ctime>
#include <functional>
#include <iostream>
#include <random>
#include <string>
#include <unordered_set>
#include <vector>

#include "sol.cpp"

template <typename Tree>
class Tester {
 public:
  Tester() {
    tree_ = Solution::Construct(static_cast<Tree*>(nullptr));
  }
  virtual void Test() {
    TestOne();
    TestTwo();
  }
  ~Tester() {
    delete tree_;
  }
 protected:
  static const int test_size = 5e3;
  static const int mod = 1e5;
  std::mt19937 gen_;
  Tree* tree_;
  std::set<int> true_tree_;
  bool test_flag_ = true;

  void TestOne() {
    test_flag_ = true;
    for (int i = 0; i < test_size; ++i) {
      if (gen_() % 2 == 0) {
        int elem = gen_() % mod;
        Insert(tree_, elem);
        true_tree_.insert(elem);
      } else {
        int elem = 0;
        if (gen_() % 3 > 0) {
          auto it = true_tree_.begin();
          std::advance(it, gen_() % (true_tree_.size() / 2 + 1));
          elem = *it;
        } else {
          elem = gen_() % mod;
        }
        if (Find(tree_, elem) != (true_tree_.find(elem) != true_tree_.end())) {
          Log(elem);
          return;
        }
      }
    }
    if (test_flag_) {
      std::cout << "Test one is correct\n";
    }
  }
  void TestTwo() {
    test_flag_ = true;
    for (int i = 0; i < test_size; ++i) {
      int rnd = gen_() % 3;
      if (rnd == 0) {
        int elem = gen_() % mod;
        Insert(tree_, elem);
        true_tree_.insert(elem);
      } else {
        int elem = 0;
        if (gen_() % 3 > 0) {
          auto it = true_tree_.begin();
          std::advance(it, gen_() % (true_tree_.size() / 2 + 1));
          elem = *it;
        } else {
          elem = gen_() % mod;
        }
        if (rnd == 1) {
          if (Find(tree_, elem) != (true_tree_.find(elem) != true_tree_.end())) {
            Log(elem);
            return;
          }
        } else {
          Erase(tree_, elem);
          true_tree_.erase(elem);
        }
      }
    }
    if (test_flag_) {
      std::cout << "Test two is correct\n";
    }
  }
  void Log(int elem) {
    test_flag_ = false;
    std::cout << "Find is incorrect! ";
    if (Find(tree_, elem)) {
      std::cout << "Find element that not in tree\n";
    } else {
      std::cout << "Don't find element that in tree\n";
    }
  }
  void TestFlag(int elem) {
    if (Find(tree_, elem) != (true_tree_.find(elem) != true_tree_.end())) {
      test_flag_ = false;
    }
  }
};

class SplayTester : public Tester<Solution::SplayTree> {
 public:
  void Test() final {
    Tester<Solution::SplayTree>::Test();
    TestThree();
  }
 private:
  void TestThree() {
    test_flag_ = true;
    std::unordered_set<int> freq_elems;
    for (int i = 0; i < test_size / 100; ++i) {
      freq_elems.insert(gen_() % mod);
    }
    for (int i = 0; i < test_size; ++i) {
      int elem;
      if (gen_() % 4 == 0) {
        elem = gen_() % mod;
        Insert(tree_, elem);
        true_tree_.insert(elem);
      } else {
        auto it = freq_elems.begin();
        std::advance(it, gen_() % (freq_elems.size()));
        Insert(tree_, *it);
        true_tree_.insert(*it);
      }
    }
    for (int i = 0; i < test_size; ++i) {
      int elem;
      if (gen_() % 10 == 0) {
        elem = gen_() % mod;
        TestFlag(elem);
      } else {
        auto it = freq_elems.begin();
        std::advance(it, gen_() % (freq_elems.size()));
        TestFlag(*it);
      }
      if (!test_flag_) {
        Log(elem);
        return;
      }
    }
    std::cout << "Test three for Splay is correct\n";
  }
};

class BTester : public Tester<Solution::BTree> {
 public:
  void Test() final {
    Tester<Solution::BTree>::Test();
    TestThree();
  }
 private:
  void TestThree() {
    test_flag_ = true;
    for (int i = 0; i < test_size; ++i) {
      int elem;
      int rnd_cmd = gen_() % 10;
      int rnd_home = gen_() % 4;
      if (rnd_cmd == 0 && rnd_home == 0) {
        elem = gen_() % mod;
        Insert(tree_, elem);
        true_tree_.insert(elem);
      } else if (rnd_cmd == 0 && rnd_home != 0) {
        auto it = true_tree_.begin();
        std::advance(it, gen_() % (true_tree_.size()));
        elem = *it;
        Insert(tree_, elem);
        true_tree_.insert(elem);
      } else if (rnd_cmd != 0 && rnd_home == 0) {
        elem = gen_() % mod;
        TestFlag(elem);
      } else {
        auto it = true_tree_.begin();
        std::advance(it, gen_() % (true_tree_.size()));
        elem = *it;
        TestFlag(elem);
      }
      if (!test_flag_) {
        Log(elem);
        return;
      }
    }
    std::cout << "Test three for B-tree is correct\n";
  }
};

int main() {
  std::cout << "Treap test\n";
  Tester<Solution::Treap> treap_tester;
  treap_tester.Test();
  std::cout << "AVL test\n";
  Tester<Solution::AVLTree> avl_tester;
  avl_tester.Test();
  std::cout << "Splay test\n";
  SplayTester splay_tester;
  splay_tester.Test();
  std::cout << "RB test\n";
  Tester<Solution::RBTree> rb_tester;
  rb_tester.Test();
  std::cout << "B test\n";
  BTester b_tester;
  b_tester.Test();
}

