# Семинар седьмой

## AVL-дерево

Это самое простое бинарное дерево поиска, гарантирующее сбалансированность. Более того, оно гарантирует, что модуль разности высот поддеревьев будет не больше 1.

*Примечание.* Высота дерева - длина самого длинного пути от корня до какого-то из листьев.

### А как поддерживать?

Введем величину $`\Delta(a) = h(a->L) - h(a->R)`$, где *a* - вершина дерева, а *L* и *R* - левое и правое поддерево соответственно. Тогда введем четыре типа поворотов, чтобы поддерживать свойство AVL-дерева.

![Turns](images/turns.png)

Тогда заметим, что мы восстановили баланс в поддеревьях. Правые повороты определяются симметрично левым.

### Вставка элемента

1. Спускаемся по дереву до вершины с одним ребенком (или до листа), идя вправо или влево в зависимости от того, больше или меньше ли вставляемый элемент значения в узле.

2. Добавляем элемент как лист.

3. Поднимаемся вверх, делая соответствующие повороты для балансировки поддеревьев.

### Удаление элемента

1. Находим элемент.

2. (а) Если у элемента не два ребенка, то просто переподвесим сына (при наличии) к родителю.

2. (б) Если у элемента два ребенка, то найдем элемент, наменьший среди больших данного (один раз вправо и влево до упора). Свапаем его с нужным, удаляем элемент (у него не больше двух детей).

3. Поднимаемся вверх (от самой нижней вершины, которую затронули), делая соответствующие повороты для балансировки поддеревьев.
 

### Поиск элемента

Легко однозначно найти элемент рекурсивным спуском по дереву. Если мы пытаемся пойти туда, где нет элементов, то данного элемента нет в дереве, иначе - мы его найдем.

### Анализ операций

Заметим, что все эти операции работают за $`\mathcal{O}(H)`$, где *H* - высота дерева, так как все повороты совершаются за $`\mathcal{O}(1)`$.

**Теорема.** Высота AVL-дерева составляет $`\mathcal{O}(\log N)`$.


## Splay-дерево

Это бинарное дерево поиска, которое гарантирует, что чем недавнее был вызван элемент, тем скорее операция с ним будет выполнена. Например, для баз данных, где запросы поиска к определенным элементам происходят часто, оно актуальнее обычного AVL-дерева.

### Вспомогательные операции 

Их всего три.

1. *Zig* если у элемента нет деда (то есть только отец), то делается поворот вокруг ребра, соединяющего элемент с родителем, чтобы сделать его корнем.

2. *Zig-zig* если у элемента есть дед, а еще отец и дед находятся по одну сторону от элемента. Тогда делаем поворот вокруг ребра *отец-дед*, а потом вокруг ребра *элемент-отец*.

3. *Zig-zag* если у элемента есть дед, а еще отец и дед находятся по разные стороны от элемента. Тогда делаем поворот вокруг ребра *элемент-отец*, а потом вокруг ребра *элемент-дед*.

### Операция Splay

Splay(x) проталкивает элемент *x* вверх с помощью одной из трех операций выше на выбор рекурсивно, пока тот не станет корнем.

### Основные операции

1. *Find(x)*. Ищет элемент как обычно, только еще вызывает Splay(x).

2. $`Merge(T_1, T_2)`$. Найдем максимум в $`T_1`$, вызываем от него Splay. Тогда он станет корнем $`T_1`$ без правого ребенка. Подвесим $`T_2`$ как правого ребенка.

3. *Split(T, x)*. Запустим Splay от *x*, вернем его поддеревья.

4. *Insert(x)*. Найдем место, куда его надо вставить листом, вызываем Splay(x).

5. *Erase(x)*. Найдем элемент, вызовем Splay(x), а потом Split(x). Осталость только вернуть Merge(x->L, x->R).

### Анализ

**Теорема.** Операция Splay работает амортизированно за $`\mathcal{O}(\log N)`$.

## ДЗ

1. Контест.

2. Теоретическое дз.